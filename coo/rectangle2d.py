"""Définition de la classe Rectangle2D"""

from coo.forme import Forme
from coo.point2d import Point2D

class Rectangle2D(Forme):
    def __init__(self, orig, tailleX, tailleY):
        """Initialise un rectangle à partir d'un point et d'une taille en X et Y"""
        self.orig = orig
        self.fin = Point2D(orig.x + tailleX, orig.y + tailleY)

    def __str__(self):
        return "Rectangle2D({}, {})".format(self.orig, self.fin)

    def translate(self, dx, dy):
        """Déplace le rectangle de dx en abscisse et de dy en ordonnée"""
        self.orig.translate(dx, dy)
        self.fin.translate(dx, dy)
